<?php
/**
 * Image Sizes Class.
 *
 * @package Kindling_Theme
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

namespace Kindling\Images;

/**
 * Handles adding custom image sizes and other global image related functionality.
 */
class ImageSizes
{
    /**
     * Sets all of the custom image sizes.
     */
    public function all()
    {
        return apply_filters('kindling_image_sizes', []);
    }

    /**
     * Registers all of the new image sizes for use in our theme
     *
     * @return self
     */
    public function register()
    {
        // First set the thumb size and make sure that this theme supports thumbs.
        if (function_exists('add_theme_support')) {
            add_theme_support('post-thumbnails');
            set_post_thumbnail_size(140, 140); // Default Post Thumbnail dimensions.
        } // if()

        // Now add the sizes.
        $this->add();

        return $this;
    }

    /**
     * Ads the image sizes.
     */
    protected function add()
    {
        if (! function_exists('add_image_size')) {
            return;
        } // if()

        // Register the image sizes.
        foreach ($sizes = $this->all() as $size) {
            $this->addSize($size);
        }

        // Add the images to the media manager image size select.
        add_filter('image_size_names_choose', function ($names) use ($sizes) {
            return array_merge($names, $this->getImageSizeNamesChoose($sizes));
        });
    }

    /**
     * Adds an image size.
     *
     * @param array $size Image size to add.
     */
    protected function addSize($size)
    {
        $size = collect($size);
        add_image_size(
            $name = $this->getName($size),
            $width = $size->get('width', ''),
            $height = $size->get('height', ''),
            $crop = $size->get('crop', true)
        );
    }

    /**
     * Get the image sizes for the media manager size select.
     *
     * @param  array $sizes Image size to add.
     *
     * @return array The image sizes for the media manager size select.
     */
    protected function getImageSizeNamesChoose($sizes)
    {
        return collect($sizes)->mapWithKeys(function ($size) {
            $size = collect($size);
            if ($size->get('enable_choose', true)) {
                return [$this->getName($size) => $this->getLabel($size)];
            }

            return ['' => null];
        })->filter()->toArray();
    }

    /**
     * Get the name from the size.
     *
     * @param array|\Illuminate\Support\Collection $size
     * @return string
     */
    protected function getName($size)
    {
        $size = is_array($size) ? collect($size) : $size;

        return $size->get('name', "{$size->get('width', '')}x{$size->get('height', '')}");
    }

    /**
     * Get the label from the size.
     *
     * @param array|\Illuminate\Support\Collection $size
     * @return string
     */
    protected function getLabel($size)
    {
        $size = is_array($size) ? collect($size) : $size;

        $label = title_case($this->getName($size));
        $label = str_replace(['-', '_'], ' ', $label);

        return $label;
    }
}
