<?php
/**
 * Kindling Images - Actions
 * @package Kindling_Images
 */

use Kindling\Images\ImageSizes;
use Illuminate\Contracts\Container\Container as ContainerContract;

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Register image sizes.
     */
    add_action('init', function () {
        // Add the image sizes to the container.
        kindling()->singleton($abstract = 'kindling-images.sizes', function (ContainerContract $app) {
            return (new ImageSizes)->register();
        });

        // Resolve the image sizes out of the container to initialize it.
        kindling()->make($abstract);
    });
});
